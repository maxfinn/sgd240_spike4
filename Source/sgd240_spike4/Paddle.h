// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "Paddle.generated.h"

UCLASS()
class SGD240_SPIKE4_API APaddle : public APawn
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	UBoxComponent* BoxComponent;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* BoxVisual;

public:
	// Sets default values for this pawn's properties
	APaddle();
	
	UPROPERTY(EditAnywhere)
	float MovementVelocity;

	UPROPERTY(EditAnywhere)
	FVector PaddleSize;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/** */
	UFUNCTION(BlueprintCallable, Category="Movement")
	void MoveY(float DeltaTime, float MovementMagnitude);
	
};
