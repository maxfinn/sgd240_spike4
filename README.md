﻿# Spike Report

## Unreal Engine Gameplay Framework

### Introduction

We’ve just done some tutorials on how to use C++ and Blueprints together, but now we need to look across the entirety of how Unreal Engine works to understand it.

Let’s build a small project in Unreal that explores each of the areas, and write a report that explains what is the purpose of (and relationships between) each of the following:

### Goals

1. We do not understand the Unreal Engine Gameplay Framework.
    * GameMode, GameModeBase, PlayerState, and GameState
    * Pawns and Characters
    * Controllers, PlayerControllers, and AIControllers
    * Camera

### Personnel

* Primary - Max Finn
* Secondary - N/A

### Technologies, Tools, and Resources used

* [Intro to C++ Programming in UE4](https://docs.unrealengine.com/latest/INT/Programming/Introduction/index.html)
* [Game Mode and Game State](https://docs.unrealengine.com/latest/INT/Gameplay/Framework/GameMode/)
* [Gameplay Timers](https://docs.unrealengine.com/latest/INT/Programming/UnrealArchitecture/Timers/)

### Tasks Undertaken

1. In an empty C++ project, create the various C++ and Blueprint classes.
    1. Create the GameStateBase and Pawn classes in C++.
    1. Create the GameModeBase and PlayerController in Blueprint. While these classes can also be implemented in C++, it proved far simpler to do them as Blueprints.
1. In GameModeBase, set the GameState, PlayerController and Default Pawn classes to our newly created classes via the dropdown menus (see figure 1).
1. In GameStateBase, add a timer which will tick each second allowing us to see the game time (see code block 1a, 1b).
1. In Pawn, add visual components in code and create each 'action' as an individual function (see code block 2a, 2b).
    1. Add a box collider and set it as the root component, followed by adding a a cube mesh component and sizing it to match the collider.
    1. For this Pawn, simply add a `MoveY` function which will translate the Pawn left and right across the screen.
1. Navigate to Edit > Project Settings > Engine > Input, and set up axis mappings to take our input (see figure 2).
1. In PlayerController, cast the reference of our Pawn to its object, and use this to call it's functions from within the PlayerController. Supply the DeltaTime from the EventTick node. Finally, link these functions with the relevant axis mappings to control when they are called (see figure 3).

### What we found out

Answering the gap questions, we found the general usage of components to be as follows:
 - The `GameMode` holds the rules for a game, such as number of players, how players are spawned and what is used to control them.
 - The `GameModeBase` is a stripped down version of GameMode for games which do not utilise EPIC's multiplayer logic.
 - The `PlayerState` holds information relevant only to each individual player, such as their individual scores, inventory and the like.
 - The `GameState` holds a list of PlayerStates, as well as any game-wide scores, such as a team's score or the game clock.
 - The `Pawn` is the physical representation of our player, and contains logic such as how it moves and other actions it can make (fighting, eating, sleeping, etc.).
 - The `Character` is an extension of the Pawn, in that it is specifically setup with movement logic and similar to represent a humanoid character.
 - The `PlayerController` takes a player's physical input, such as that from a keyboard, and converts it into the Pawn's actions. The player's intent.
 - The `AIController` functions like a PlayerController, but rather than take input from a human player, it generates input itself based on various functions. Useful for creating NPCs.
 - The `Camera` is what you would expect; a virtual camera which gains us a view into the game world.

Once again, there were many issues that cropped up during this spike. Initially, I had aimed to produce each of the four components in C++, but in the case of both the GameModeBase and PlayerController, I ran into unexpected issues being able to make them work, which I have yet to resolve. In addition, it seemed as though documentation was lacking in unexpected areas. Of particular note was the PlayerController, as all the official tutorials break their own rules and handle input through the Pawn class directly instead. Thankfully, the Blueprints are straightforward enough that even with less than ideal documentation, working components could be made.

On a different note, I briefly had troubles with being unable to get the PlayerController to attach itself to the Pawn. In the end, this was a self-inflicted error where I had attached a visual component to a Pawn instance placed in the scene, but when the scene began, it was creating a new Pawn instance to control, which lacked a visual component.

The GameStateBase class gave me particular trouble as well, as I couldn't find documentation anywhere for the implementation of timers, but as it turned out, had I looked through all of the supplied resources with this spike, I would have seen that there was a complete tutorial included. Moral of the story is of course to go over the resources provided upfront before rushing out to look elsewhere.

### Open Issues/Risks

1. While each of the components function as intended, I feel that the PlayerController could have possibly been simplified in someway.

### Appendix

Code block 1a
```c++
// MyGameStateBase.h

class SGD240_SPIKE4_API AMyGameStateBase : public AGameStateBase
{
    GENERATED_BODY()

public:
    AMyGameStateBase();

    UPROPERTY(VisibleAnywhere)
    uint32 GameTime;

    FTimerHandle TimeUpdateHandle;

    virtual void BeginPlay();
    
    void UpdateGameTime();
};
```
Code block 1b
```c++
// MyGameStateBase.cpp

void AMyGameStateBase::BeginPlay()
{
    GetWorldTimerManager().SetTimer(TimeUpdateHandle, this, &AMyGameStateBase::UpdateWorldTime, 1.0f, true, 0.0f);    
}

void AMyGameStateBase::UpdateWorldTime()
{
    GameTime = (int32)GetServerWorldTimeSeconds();
}
```
Code block 2a
```c++
// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "Paddle.generated.h"

UCLASS()
class SGD240_SPIKE4_API APaddle : public APawn
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
	UBoxComponent* BoxComponent;

	UPROPERTY(EditAnywhere)
	UStaticMeshComponent* BoxVisual;

public:
	// Sets default values for this pawn's properties
	APaddle();
	
	UPROPERTY(EditAnywhere)
	float MovementVelocity;

	UPROPERTY(EditAnywhere)
	FVector PaddleSize;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/** */
	UFUNCTION(BlueprintCallable, Category="Movement")
	void MoveY(float DeltaTime, float MovementMagnitude);
	
};

```
Code block 2b
```c++
// Fill out your copyright notice in the Description page of Project Settings.

#include "sgd240_spike4.h"
#include "Paddle.h"

// Sets default values
APaddle::APaddle()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Paddle size in centimetres
	PaddleSize = FVector(25.0f, 150.0f, 25.0f);
	MovementVelocity = 500.0f;

	// Our root component will be a box that reacts to physics
	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("RootComponent"));
	RootComponent = BoxComponent;
	BoxComponent->InitBoxExtent(PaddleSize / 2.0f);
	BoxComponent->SetCollisionProfileName(TEXT("Pawn"));

	// Create and position a mesh component for the paddle
	BoxVisual = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("VisualRepresentation"));
	BoxVisual->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UStaticMesh> BoxVisualAsset(TEXT("/Game/Cube.Cube"));
	if (BoxVisualAsset.Succeeded())
	{
		BoxVisual->SetStaticMesh(BoxVisualAsset.Object);
		BoxVisual->SetRelativeLocation(FVector(0.0f, 0.0f, 0.0f));
		BoxVisual->SetWorldScale3D(PaddleSize / 100.0f);
	}

}

// Called when the game starts or when spawned
void APaddle::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APaddle::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APaddle::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void APaddle::MoveY(float DeltaTime, float MovementMagnitude)
{
	FVector NewLocation = GetActorLocation() + FVector(0.0f, MovementMagnitude * MovementVelocity * DeltaTime, 0.0f);
	NewLocation.Y = FMath::Clamp(NewLocation.Y, -425.f, 425.0f);
	SetActorLocation(NewLocation);

}
```
Figure 1: The GameModeBase Blueprint with correct classes set

![GameModeBase Blueprint][GameModeBase01]

Figure 2: Axis mappings added for keyboard and game controller; mouse movement wouldn't make sense in the context of this game

![Axis Mappings][AxisMappings01]

Figure 3: The PlayerController Blueprint taking an axis mapping and using it to run one of the Pawn's functions.

![PlayerController Blueprint][PlayerController01]

[GameModeBase01]: https://monosnap.com/file/gBUNmkLBkOSh8rbFWHKrl28WKGmlK5.png "GameModeBase setup"
[AxisMappings01]: https://monosnap.com/file/hkJ1H6EHii48srDBJmrjnaTIcQFd8G.png "Axis Mappings"
[PlayerController01]: https://monosnap.com/file/OmCY4W5bBmFwCiIbaPI3jzTXqUXnM5.png "PlayerController setup"